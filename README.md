# LoRaWAN Soil Moisture Monitor

Soil moisture sensors measure the volumetric water content in soil. Since the direct gravimetric measurement of free soil moisture requires removing, drying, and weighing of a sample, soil moisture sensors measure the volumetric water content indirectly by using some other property of the soil, such as electrical resistance, dielectric constant, or interaction with neutrons, as a proxy for the moisture content. Here we are using a capacitive type soil moisture sensor and controller board as [ULP Lora 2.1 board](https://gitlab.com/icfoss/OpenIoT/ulplora). The sensor is used to collect the moisture data and the board's inbuilt rf module used to send the data to the nearest gateway and this data is pushed into the influx Db, which saves the data for data acquisition! The soil moisture device is working in low power mode. So that it is powered by a single battery of 4.2 V.As we work through the project we will connect up the various sensors. In this process, we create a software sketch that will run the soil moisture.

## Getting Started


- Make sure that you have a ULP LoRa Board.

- Install project library from [here](https://gitlab.com/arunchandra2500/lorawan-soil_moisture-monitor/-/tree/master/libraries) . Copy the library to ~/Arduino/libraries/

- Select board : Arduino Pro Mini 3.3v 8Mhz

## Prerequisites

Arduino IDE - 1.8.9 [Tested]

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/arunchandra2500/lorawan-soil_moisture-monitor/-/blob/master/LICENSE.md) file for details
